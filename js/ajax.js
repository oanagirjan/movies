/**
 * Created by ogirjan on 02.04.2018.
 */


export function get(url){
    let settings = {
        "async": true,
        "crossDomain": true,
        "url": url,
        "method": "GET",
        "headers": {},
        "data": "{}"
    };

    let d = new $.Deferred

    $.ajax(settings).then(response => {
        d.resolve(response)
    });

    return d.promise()
}