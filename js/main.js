import * as ajax from './ajax'
$( document ).ready( () =>{
    let favorites=JSON.parse(sessionStorage.getItem('favorites')) || []
    let $movieList = $('.movies-list');

    // $('#search-input').blur( () =>
    // {
    //     if( !$(this).val() ) {
    //         $movieList.empty()
    //         let values = Object.values(favorites)
    //         values.map(
    //             item => {
    //                 movie(item)
    //             })
    //     }
    // });

    $movieList.empty()

    favorites.map(
        favorite => {
            movie(favorite)
        })

    $('.search-button').click(
        searchFunction()
    )
    $('.search-input').keypress((e) => {
        if(e.which === 13)
            searchFunction()
    });


    function movie(movieDetails) {
        let className = `movie-item-${movieDetails.id}`
        let poster =`<img class="mr-3" src="http://image.tmdb.org/t/p/w185/${movieDetails.poster_path}" >`
        let details = `<span>${movieDetails.overview}</span>`
        $movieList.append(`
            <div class="media ${className} movie-item">
                ${poster}
                <div class="media-body">
                    <a href="#" class="movie-title">
                        <h5 class="mt-0">${movieDetails.title}</h5>
                    </a>
                    ${details}
                </div>
            </div>
        `)
        $(`.${className} .movie-title`).click(
            ()=>{
                $('#movie-modal .movie-modal-title').text(movieDetails.title)
                $('#movie-modal .poster').empty()
                $('#movie-modal .poster').append(poster)
                $('#movie-modal .details').empty()
                $('#movie-modal .details').append(details)
                $('.add-to-favorites-button').click(
                    () =>{
                        favorites[movieDetails.id] = movieDetails
                        sessionStorage.setItem('favorites', JSON.stringify(favorites))
                    }
                )
                $('#movie-modal').modal('show');
            }
        )
    }

    function searchFunction(){
        let query = $('#search-input').val()
        let searchUrl = `https://api.themoviedb.org/3/search/movie?query=${query}&api_key=68a83b6cad8e302a479486cec3184fca`;
        let promise = ajax.get(searchUrl);
        promise.then( response =>{
            console.log('search result', response);
            $movieList.empty()
            response.results.map(
                result => {
                    let movieUrl = `https://api.themoviedb.org/3/movie/${result.id}?api_key=68a83b6cad8e302a479486cec3184fca`
                    let promise = ajax.get(movieUrl);
                    promise.then( response =>{
                        console.log('movie result', response);
                        movie(response)
                    })
                })
        })
    }

})